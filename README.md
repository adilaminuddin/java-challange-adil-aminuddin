# java-challange-adil-aminuddin

### Java Challange for IST Backend Test

## Project About
This project created just for test/challanging, Simple Java Spring Boot REST-API AUTH With Postgresql

## API link
1. Login -> [POST] http://localhost:8080/api/v1/login (status = fail).
2. Register -> [POST] http://localhost:8080/api/v1/register (status = success).
3. List User -> [GET] http://localhost:8080/api/v1/users/list (status = success).
4. Delete User -> [Delete] http://localhost:8080/api/v1/users/delete/{id} (status = success).
5. Update User -> [Patch] http://localhost:8080/api/v1/users/update/{id} (status = success).

## WARNING. THIS APP UNDER CONSTRUCTION.