package ist.challange.adil_aminuddin.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ist.challange.adil_aminuddin.Dto.RegisterDto;
import ist.challange.adil_aminuddin.Entity.User;
import ist.challange.adil_aminuddin.Exception.GlobalExceptionHandler;
import ist.challange.adil_aminuddin.Exception.ResourceNotFoundException;
import ist.challange.adil_aminuddin.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    private GlobalExceptionHandler handler;


    @PostMapping("/login")
    public ResponseEntity<?> loginUser(
            @Valid @RequestBody User user) {
        System.out.println("login user: " + user.toString());
        List<User> list = userRepository.findAll();
        System.out.println("list users: " + list.toString());
          for (User s : list){
            if (s.equals(user)){
                System.out.println("login users: " + s.toString());
                user.setLoggedIn(true);
                userRepository.save(user);
                return new ResponseEntity<>("Login Sukses!", HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("Login Gagal!",HttpStatus.UNAUTHORIZED);
    }

    @PostMapping("/register")
    public ResponseEntity<?> createUser(@RequestBody RegisterDto registerDto) {
        // add check for username exists in a DB
        if(userRepository.existsByUsername(registerDto.getUsername())){
            return new ResponseEntity<>("Response 409, Username Sudah terpakai", HttpStatus.CONFLICT);
//            return new ResponseEntity<>("Username Sudah Terpakai!", HttpStatus.CONFLICT);
        }
        // create user object
        User user = new User();
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setLoggedIn(false);

        userRepository.save(user);
        return new ResponseEntity<>("Pendaftaran Berhasil", HttpStatus.OK);
    }
    @PostMapping("/users/logout")
    public ResponseEntity<?> logUserOut(@Valid @RequestBody User user) {
        List<User> users = userRepository.findAll();
        for (User other : users) {
            if (other.equals(user)) {
                user.setLoggedIn(false);
                userRepository.save(user);
                return new ResponseEntity<>("Logout Berhasil", HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("Logout Gagal", HttpStatus.EXPECTATION_FAILED);
    }

    @GetMapping("/users/list")
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<User> getUserById(@PathVariable(value = "id") Long id)
            throws ResourceNotFoundException {

        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException
                        ("User not found for this id :: " + id));
        return ResponseEntity.ok().body(user);
    }


    @PatchMapping("/users/update/{id}")
    public ResponseEntity<User> updateUser(@PathVariable(value = "id") Long id, @RequestBody User userU)
            throws ResourceNotFoundException {

        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException
                        ("User not found for this id :: " + id));

        user.setUsername(userU.getUsername());
        user.setPassword(userU.getPassword());
        user.setId(id);
        final User updateUser = userRepository.save(user);
        return new ResponseEntity<>(updateUser, HttpStatus.OK);
    }

    @DeleteMapping("/users/delete/{id}")
    public Map<String, Boolean> deleteUser(@PathVariable(value = "id")
                                                   Long id) throws ResourceNotFoundException {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException
                        ("User not found for this id :: " + id));

        userRepository.delete(user);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
