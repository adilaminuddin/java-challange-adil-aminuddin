package ist.challange.adil_aminuddin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdilAminuddinApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdilAminuddinApplication.class, args);
    }

}
