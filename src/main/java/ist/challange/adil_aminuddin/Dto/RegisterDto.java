package ist.challange.adil_aminuddin.Dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class RegisterDto {

    @NotBlank(message = "Username is required.")
    @Size(min = 5, max = 25, message = "Out Of requirement.")
    @NotEmpty
    private String username;

    @NotBlank(message = "Password is required.")
    @Size(min = 5, max = 25, message = "Out Of requirement.")
    @NotEmpty
    private String password;


    private boolean loggedIn;
}
