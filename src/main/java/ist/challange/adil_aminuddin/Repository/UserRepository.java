package ist.challange.adil_aminuddin.Repository;

import ist.challange.adil_aminuddin.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Long> {

//    @Query("Select u from users u WHERE u.username=:username")
    Boolean existsByUsername(String username);
//    @Query(value="Select u.* from users u WHERE u.username=?1 AND u.password=?2",nativeQuery = true)

//    @Query("UPDATE users u SET logged_in=true FROM (Select * from users d WHERE d.username=?) AS upd WHERE u.id=upd.id")
    String findByUsername(String username);
}
